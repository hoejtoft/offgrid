@extends('master')
@section('title', 'Pangaea - add section')
@section('content')
    <div class="container container-top">
        <div class="row">
            <!-- form input -->
            <form class="adminform" method="post" enctype="multipart/form-data" action="{{action('SectionController@update', $id)}}">
                {{csrf_field()}}
                {{ method_field('PUT')}}
                <h1 class="text-center Headline">Edit page section</h1>
                @if(count($errors) > 0)

                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                <!--  General -->
                <div class="form-group">
                    <h2 class="heading full">General</h2>
                    <div class="controls full">
                        <input type="text" id="headline" value="{{ $pagesection->headline }}" class="floatLabel" name="headline">
                        <label for="firstName">Article headline</label>
                    </div>
                    <div class="controls full">
                        <textarea name="sectiontxt" class="floatLabel" id="sectiontxt">{{ $pagesection->sectiontxt }}</textarea>
                        <label for="sectiontxt">Article teaser text</label>
                    </div>
                    <div class="controls full">

                        <h2 class="heading">Section content</h2>
                        <textarea id="editor" name="editor">{{ $pagesection->sectioncontent }}</textarea>
                        <input type="hidden" name="sectioncontent" value="{{ $pagesection->sectioncontent }}" id="sectioncontent">
                    </div>
                    <div class="controls full">
                        <input type="text" id="imgname_1" class="floatLabel" value="{{$pagesection->imgname_1}}" name="imgname_1">
                        {{--<input type="file" id="imgname_1" class="floatLabel" name="imgname_1" value="{{$pagesection->imgname_1}}">--}}
                        <label for="imgname_1">Article image</label>
                        <div class="imgpreview">

                            <img src="{{asset('connect/uploads/images/'.$pagesection->imgname_1) }}">
                        </div>
                    </div>
                </div>
                <!--  Details -->
                <div class="form-group">
                    <h2 class="heading full">Details</h2>
                    <div class="controls full">
                        <select class="floatLabel" name="buttonlink">
                            <option value="blank"></option>
                            <option value="/" {{($pagesection->buttonlink == "/") ? 'selected' : ''}}>Frontpage</option>
                        </select>
                        <label for="buttonlink">Button link</label>
                    </div>
                    <div class="controls full">
                        <select class="floatLabel" name="pagecategory">
                            <option value="blank"></option>
                            <option value="Frontpage" {{($pagesection->pagecategory == "Frontpage") ? 'selected' : ''}}>Frontpage</option>
                            <option value="Offgrid" {{($pagesection->pagecategory == "Offgrid") ? 'selected' : ''}}>Off-grid living</option>
                        </select>
                        <label for="pagecategory">Page placement</label>
                    </div>
                    <div class="controls full">
                        <select class="floatLabel" name="sectiontype">
                            <option value="blank"></option>
                            <option value="0" {{($pagesection->sectiontype == 0) ? 'selected' : ''}}>Text only</option>
                            <option value="1" {{($pagesection->sectiontype == 1) ? 'selected' : ''}}>Text right - Image left</option>
                            <option value="2" {{($pagesection->sectiontype == 2) ? 'selected' : ''}}>Text left - Image right</option>
                        </select>
                        <label for="sectiontype">Section type</label>
                        <button class="full">Update section</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection