@extends('master')
@section('title', 'Pangaea - off-grid')
@section('indexcontent')


<div class="container">
                <div class="block">
                    @foreach($PageSection as $row)
                        {{--Sectiontype 0 = text only--}}
                        {{--Sectiontype 1 = text right--}}
                        {{--Sectiontype 2 = text left--}}
                        @if($row->sectiontype == 0)
                            <div class="row nomargin pagesection">
                                <div class="content-heading pull-right"><h2>{{$row->headline}} </h2></div>
                                <div class="span4">

                                    <div class="article-text-full pull-right">
                                        <p>
                                            {{$row->sectiontxt}}
                                        </p>
                                        <div class="readmore">
                                            <a class="readmore-btn" data-toggle="modal" data-target="#fsModal{{$row->id}}">Read more</a>
                                        </div>
                        @if(auth()->guest())
                        @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok pull-left">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('SectionController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('SectionController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                                    </div>
                                </div>
                            </div>
                        @elseif($row->sectiontype == 1)
                            <div class="row nomargin pagesection">
                                <div class="span4">


                                    <img class="img-left" src="{{asset('connect/uploads/images/'.$row->imgname_1) }}">
                                    <div class="content-heading pull-right"><h2>{{$row->headline}} </h2></div>

                                    <div class="article-text pull-right">
                                        <p>
                                            {{$row->sectiontxt}}
                                        </p>
                                        <div class="readmore">
                                            <a class="readmore-btn" data-toggle="modal" data-target="#fsModal{{$row->id}}">Read more</a>
                                        </div>
                                        @if(auth()->guest())
                        @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok pull-right">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('SectionController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('SectionController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                                    </div>
                                </div>
                            </div>
                        @elseif($row->sectiontype == 2)
                            <div class="row nomargin pagesection">
                                <div class="span4">
                                    <img class="img-right" src="{{asset('connect/uploads/images/'.$row->imgname_1) }}">

                                    <div class="content-heading pull-left"><h2>{{$row->headline}} </h2></div>
                                    <div class="article-text pull-left">
                                        <p>
                                            {{$row->sectiontxt}}
                                        </p>
                                        <div class="readmore">
                                            <a class="readmore-btn" data-toggle="modal" data-target="#fsModal{{$row->id}}">Read more</a>
                                        </div>
                                        @if(auth()->guest())
                        @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok pull-left">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('SectionController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('SectionController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                                <!-- modal -->
                            {{--<div class="modal fade modal-fullscreen" id="fsModal{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">--}}
                                {{--<div class="modal-dialog modal-full">--}}
                                    {{--<div class="modal-content modal-content-one">--}}
                                        {{--<div class="modal-header">--}}
                                            {{--<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>--}}
                                            {{--<h4 class="modal-title" id="myModalLabel">{{$row->headline}}</h4>--}}
                                        {{--</div>--}}
                                        {{--<div class="modal-body">--}}
                                            {{--{!!$row->sectioncontent!!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="portfolio-modal section-modal modal fade" id="fsModal{{$row->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog nomargin">
                                    <div class="modal-content">
                                        <div class="close-modal" data-dismiss="modal">
                                            <div class="lr">
                                                <div class="rl"></div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-lg-12 mx-auto">
                                                    <div class="modal-body">
                                                        <!-- Project Details Go Here -->
                                                        <h2 class="text-uppercase modal_headline">{{$row->headline}}</h2>

                                                        <div>{!!$row->sectioncontent!!}</div>
                                                        <button class="btn btn-primary pull-right" data-dismiss="modal" type="button">
                                                            <i class="fas fa-times"></i>
                                                            Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- modal -->

                    @endforeach

    </div>
</div>




@endsection