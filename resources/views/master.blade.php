<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"  crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/jquery.easing.js')}}"></script>

    <!-- Theme style  -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jodit/2.5.61/jodit.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jodit/2.5.61/jodit.min.js"></script>

</head>
<body>
<nav id="cd-vertical-nav">
    <ul>
        <li>
            <a href="#start" data-number="1">
                <span class="cd-dot"></span>
                <span class="cd-label">Start</span>
            </a>
        </li>
        <li>
            <a href="#portfolio" data-number="2">
                <span class="cd-dot"></span>
                <span class="cd-label">portfolio</span>
            </a>
        </li>
        <li>
            <a href="#about" data-number="3">
                <span class="cd-dot"></span>
                <span class="cd-label">about</span>
            </a>
        </li>
        <li>
            <a href="#collaborators" data-number="4">
                <span class="cd-dot"></span>
                <span class="cd-label">Portfolio</span>
            </a>
        </li>

        {{--<li>--}}
            {{--<a href="#section5" data-number="5">--}}
                {{--<span class="cd-dot"></span>--}}
                {{--<span class="cd-label">Pricing</span>--}}
            {{--</a>--}}
        {{--</li>--}}
        {{--<li>--}}
            {{--<a href="#section6" data-number="6">--}}
                {{--<span class="cd-dot"></span>--}}
                {{--<span class="cd-label">Contact</span>--}}
            {{--</a>--}}
        {{--</li>--}}
    </ul>
</nav>
<nav class="navbar navbar-expand-md navbar-light custom-nav {{(!Request::is('/')) ? 'solidnav' : ''}}" id="banner">
    <div class="container">
        <!-- Brand -->
        <a class="navbar-brand" href="/"><span class="logo">Pangaea</span></a>

        <!-- Toggler/collapsibe Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"><i class="fa fa-navicon"></i></span>
        </button>

        <!-- Navbar links -->
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav ml-auto hidelinks">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        Activities
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Bootcamp</a>
                        <a class="dropdown-item" href="#">Yoga weekend</a>
                        <a class="dropdown-item" href="#">Physical training</a>
                        <a class="dropdown-item" href="#">Plant a tree</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Products</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/Offgrid">off-grid living</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About us & philosophy</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Direction</a>
                </li>
                @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        Admin panel
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="/addsection">Create new section</a>
                    </div>
                </li>
              @endif
                <!-- Dropdown -->

            </ul>
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest

                @if (Route::has('register'))

                @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest
            </ul>
        </div>
    </div>
</nav>

@yield('indexcontent')
@yield('content')

<footer>
    <div class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
                    <h3> Contact </h3>
                    <ul>
                        <li> <i class="fas fa-phone"></i> +212 44 55 66 33 </li>
                        <li> <i class="fas fa-envelope"></i> info@Pangaea.com </li>
                    </ul>
                </div>
                <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
                    <h3> Faq </h3>
                    <ul>
                        <li> <a href="#"> Lorem Ipsum </a> </li>
                        <li> <a href="#"> Lorem Ipsum </a> </li>
                        <li> <a href="#"> Lorem Ipsum </a> </li>
                        <li> <a href="#"> Lorem Ipsum </a> </li>
                    </ul>
                </div>
                <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
                    <h3> Quick links </h3>
                    <ul>
                        <li> <a href="/register"> Register </a> </li>
                        <li> <a href="/login"> Login </a> </li>
                        <li> <a href="#"> Lorem Ipsum </a> </li>
                        <li> <a href="#"> Lorem Ipsum </a> </li>
                    </ul>
                </div>
<div class="col-lg-offset-3"></div>
                <div class="col-lg-6  col-md-6 col-sm-6 col-xs-6 ">
                    <h3 class="text-center"> Signup for newsletter </h3>
                    <ul>
                        <li>
                            <div class="input-append newsletter-box text-center">
                                <form>


                                    <div class="field email-box">
                                        <input type="text" id="email" placeholder="name@email.com"/>
                                        <label for="email">Email</label>
                                        <span class="ss-icon">check</span>
                                    </div>



                                    <input class="button" type="submit" value="Send" />
                                </form>


                            </div>
                        </li>
                    </ul>

                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </div>
    <!--/.footer-->

    <div class="footer-bottom">
        <div class="container">
            <p class="pull-left"> Copyright © Pangaea 2019. </p>
            <div class="pull-right">
                <ul class="social">
                    <li> <a href="#"> <i class="fab fa-facebook"></i> </a> </li>
                    <li> <a href="#"> <i class="fab fa-instagram"></i> </a> </li>
                    <li> <a href="#"> <i class="fab fa-pinterest-square"></i> </a> </li>
                    <li> <a href="#"> <i class="fab fa-youtube-square"></i> </a> </li>
                </ul>
            </div>
        </div>
    </div>
    <!--/.footer-bottom-->
</footer>

</body>
</html>

