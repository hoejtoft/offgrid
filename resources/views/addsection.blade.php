@extends('master')
@section('title', 'Pangaea - add section')
@section('content')

    <div class="container container-top">
        <div class="row">
            <!-- form input -->
            <form class="adminform" method="post" enctype="multipart/form-data" action="{{url('addnewsection')}}">
                {{csrf_field()}}
                <h1 class="text-center Headline">Add new page section</h1>
                @if(count($errors) > 0)

                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                <!--  General -->
                <div class="form-group">
                    <h2 class="heading full">General</h2>
                    <div class="controls full">
                        <input type="text" id="headline" value="{{ old('headline') }}" class="floatLabel" name="headline">
                        <label for="firstName">Article headline</label>
                    </div>
                    <div class="controls full">
                        <textarea name="sectiontxt" class="floatLabel" id="sectiontxt">{{ old('sectiontxt') }}</textarea>
                        <label for="sectiontxt">Article teaser text</label>
                    </div>
                    <div class="controls full">
                        {{--<textarea name="sectioncontent" class="floatLabel" id="sectioncontent">{{ old('sectioncontent') }}</textarea>--}}
                        <label for="sectioncontent">Full article text</label>
                        <textarea id="editor" name="editor"></textarea>
                        <input type="hidden" name="sectioncontent" id="sectioncontent">
                    </div>
                    <div class="controls full">
                        <input type="file" id="imgname_1" class="floatLabel" name="imgname_1">
                        <label for="imgname_1">Article image</label>
                    </div>
                </div>
                <!--  Details -->
                <div class="form-group">
                    <h2 class="heading full">Details</h2>
                    <div class="controls full">
                        <select class="floatLabel" name="buttonlink">
                            <option value="blank"></option>
                            <option value="/">Frontpage</option>
                        </select>
                        <label for="buttonlink">Button link</label>
                    </div>
                    <div class="controls full">
                        <select class="floatLabel" name="pagecategory">
                            <option value="blank"></option>
                            <option value="Frontpage">Frontpage</option>
                            <option value="Offgrid">Off-grid living</option>
                        </select>
                        <label for="pagecategory">Page placement</label>
                    </div>
                    <div class="controls full">
                        <select class="floatLabel" name="sectiontype">
                            <option value="blank"></option>
                            <option value="0">Text only</option>
                            <option value="1">Text right - Image left</option>
                            <option value="2">Text left - Image right</option>
                        </select>
                        <label for="sectiontype">Section type</label>

                        <button class="full">Add section</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection