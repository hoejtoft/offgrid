@extends('master')
@section('title', 'Pangaea - Home')
@section('indexcontent')

            <!-- Carousel -->
            <section id="start" class="cd-section">

            <div id="carousel-example-generic" class="carousel slide carousel-fade cd-section" data-ride="carousel">
                <!-- Indicators -->
                {{--<ol class="carousel-indicators">--}}
                    {{--<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>--}}
                    {{--<li data-target="#carousel-example-generic" data-slide-to="1"></li>--}}
                    {{--<li data-target="#carousel-example-generic" data-slide-to="2"></li>--}}
                {{--</ol>--}}
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="{{asset('images/1.jpg')}}" alt="First slide">
                        <!-- Static Header -->
                        <div class="header-text hidden-xs">
                            {{--<div class="col-md-12 text-center">--}}

                                {{--<br>--}}
                                {{--<h3>--}}
                                    {{--<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>--}}
                                {{--</h3>--}}
                                {{--<br>--}}
                                {{--<div class="Calltoaction-btn">--}}
                                    {{--<a class="btn btn-theme btn-sm btn-min-block" href="#">Book a lunch</a></div>--}}
                            {{--</div>--}}
                        </div><!-- /header-text -->
                    </div>
                    {{--<div class="item">--}}
                        {{--<img src="{{asset('images/2.jpg')}}" alt="Second slide">--}}
                        {{--<!-- Static Header -->--}}
                        {{--<div class="header-text hidden-xs">--}}
                            {{--<div class="col-md-12 text-center">--}}

                                {{--<br>--}}
                                {{--<h3>--}}
                                    {{--<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>--}}
                                {{--</h3>--}}
                                {{--<br>--}}
                                {{--<div class="Calltoaction-btn">--}}
                                    {{--<a class="btn btn-theme btn-sm btn-min-block" href="#">Join our bootcamp</a></div>--}}
                            {{--</div>--}}
                        {{--</div><!-- /header-text -->--}}
                    {{--</div>--}}
                    {{--<div class="item">--}}
                        {{--<img src="{{asset('images/3.jpg')}}" alt="Third slide">--}}
                        {{--<!-- Static Header -->--}}
                        {{--<div class="header-text hidden-xs">--}}
                            {{--<div class="col-md-12 text-center">--}}

                                {{--<br>--}}
                                {{--<h3>--}}
                                    {{--<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>--}}
                                {{--</h3>--}}
                                {{--<br>--}}
                                {{--<div class="Calltoaction-btn">--}}
                                    {{--<a class="btn btn-theme btn-sm btn-min-block" href="#">Get a yoga lesson</a></div>--}}
                            {{--</div>--}}
                        {{--</div><!-- /header-text -->--}}
                    {{--</div>--}}
                </div>
                <!-- Controls -->
                {{--<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">--}}
                    {{--<span class="glyphicon glyphicon-chevron-left"></span>--}}
                {{--</a>--}}
                {{--<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">--}}
                    {{--<span class="glyphicon glyphicon-chevron-right"></span>--}}
                {{--</a>--}}
            </div><!-- /carousel -->
                <a href="#portfolio" class="cd-scroll-down"><i class="fas fa-chevron-down"></i></a>
            </section>
            <!-- ONEPAGER -->

            <!-- Services -->


            <!-- Portfolio Grid -->
            <section class="bg-light cd-section" id="portfolio">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2 class="section-heading text-uppercase">Portfolio</h2>
                            <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                        </div>
                    </div>
                    <div class="row">
                        <!-- overlay effect -->
                        <div class="grid">
                            <figure class="effect-layla">
                                <img src="{{asset('images/img/portfolio/01-thumbnail.jpg')}}" alt="img06"/>
                                <figcaption>
                                    <h2>Threads</h2>
                                    <p class="layla_text">Read more about threads</p>
                                    <a data-toggle="modal" href="#portfolioModal1">View more</a>

                                </figcaption>
                            </figure>
                            <figure class="effect-layla">
                                <img src="{{asset('images/img/portfolio/02-thumbnail.jpg')}}" alt="img03"/>
                                <figcaption>
                                    <h2>Explore</h2>
                                    <p class="layla_text">Read more about exploration</p>
                                    <a data-toggle="modal" href="#portfolioModal2">View more</a>
                                </figcaption>
                            </figure>
                            <figure class="effect-layla">
                                <img src="{{asset('images/img/portfolio/03-thumbnail.jpg')}}" alt="img03"/>
                                <figcaption>
                                    <h2>Finish</h2>
                                    <p class="layla_text">Read more about fnish</p>
                                    <a data-toggle="modal" href="#portfolioModal3">View more</a>
                                </figcaption>
                            </figure>
                            <figure class="effect-layla">
                                <img src="{{asset('images/img/portfolio/04-thumbnail.jpg')}}" alt="img03"/>
                                <figcaption>
                                    <h2>Lines</h2>
                                    <p class="layla_text">Read more about lines</p>
                                    <a data-toggle="modal" href="#portfolioModal4">View more</a>
                                </figcaption>
                            </figure>
                            <figure class="effect-layla">
                                <img src="{{asset('images/img/portfolio/05-thumbnail.jpg')}}" alt="img03"/>
                                <figcaption>
                                    <h2>Southwest</h2>
                                    <p class="layla_text">Read more about Southwest</p>
                                    <a data-toggle="modal" href="#portfolioModal5">View more</a>
                                </figcaption>
                            </figure>
                            <figure class="effect-layla">
                                <img src="{{asset('images/img/portfolio/06-thumbnail.jpg')}}" alt="img03"/>
                                <figcaption>
                                    <h2>Window</h2>
                                    <p class="layla_text">Read more about Window</p>
                                    <a data-toggle="modal" href="#portfolioModal6">View more</a>
                                </figcaption>
                            </figure>
                        </div>
                        <!-- overlay effect -->
                        {{--<div class="col-md-4 col-sm-6 portfolio-item">--}}
                            {{--<a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">--}}
                                {{--<div class="portfolio-hover">--}}
                                    {{--<div class="portfolio-hover-content">--}}
                                        {{--<i class="fas fa-plus fa-3x"></i>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<img class="img-fluid" src="{{asset('images/img/portfolio/01-thumbnail.jpg')}}" alt="">--}}
                            {{--</a>--}}
                            {{--<div class="portfolio-caption">--}}
                                {{--<h4>Threads</h4>--}}
                                {{--<p class="text-muted">Illustration</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4 col-sm-6 portfolio-item">--}}
                            {{--<a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">--}}
                                {{--<div class="portfolio-hover">--}}
                                    {{--<div class="portfolio-hover-content">--}}
                                        {{--<i class="fas fa-plus fa-3x"></i>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<img class="img-fluid" src="{{asset('images/img/portfolio/02-thumbnail.jpg')}}" alt="">--}}
                            {{--</a>--}}
                            {{--<div class="portfolio-caption">--}}
                                {{--<h4>Explore</h4>--}}
                                {{--<p class="text-muted">Graphic Design</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4 col-sm-6 portfolio-item">--}}
                            {{--<a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">--}}
                                {{--<div class="portfolio-hover">--}}
                                    {{--<div class="portfolio-hover-content">--}}
                                        {{--<i class="fas fa-plus fa-3x"></i>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<img class="img-fluid" src="{{asset('images/img/portfolio/03-thumbnail.jpg')}}" alt="">--}}
                            {{--</a>--}}
                            {{--<div class="portfolio-caption">--}}
                                {{--<h4>Finish</h4>--}}
                                {{--<p class="text-muted">Identity</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4 col-sm-6 portfolio-item">--}}
                            {{--<a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">--}}
                                {{--<div class="portfolio-hover">--}}
                                    {{--<div class="portfolio-hover-content">--}}
                                        {{--<i class="fas fa-plus fa-3x"></i>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<img class="img-fluid" src="{{asset('images/img/portfolio/04-thumbnail.jpg')}}" alt="">--}}
                            {{--</a>--}}
                            {{--<div class="portfolio-caption">--}}
                                {{--<h4>Lines</h4>--}}
                                {{--<p class="text-muted">Branding</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4 col-sm-6 portfolio-item">--}}
                            {{--<a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">--}}
                                {{--<div class="portfolio-hover">--}}
                                    {{--<div class="portfolio-hover-content">--}}
                                        {{--<i class="fas fa-plus fa-3x"></i>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<img class="img-fluid" src="{{asset('images/img/portfolio/05-thumbnail.jpg')}}" alt="">--}}
                            {{--</a>--}}
                            {{--<div class="portfolio-caption">--}}
                                {{--<h4>Southwest</h4>--}}
                                {{--<p class="text-muted">Website Design</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4 col-sm-6 portfolio-item">--}}
                            {{--<a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">--}}
                                {{--<div class="portfolio-hover">--}}
                                    {{--<div class="portfolio-hover-content">--}}
                                        {{--<i class="fas fa-plus fa-3x"></i>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<img class="img-fluid" src="{{asset('images/img/portfolio/06-thumbnail.jpg')}}" alt="">--}}
                            {{--</a>--}}
                            {{--<div class="portfolio-caption">--}}
                                {{--<h4>Window</h4>--}}
                                {{--<p class="text-muted">Photography</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>

            </section>

            <!-- About -->
            <section id="about" class="cd-section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2 class="section-heading text-uppercase">About</h2>
                            <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="timeline">
                                <li>
                                    <div class="timeline-image">
                                        <img class="rounded-circle img-fluid" src="{{asset('images/img/about/1.jpg')}}" alt="">
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4>2009-2011</h4>
                                            <h4 class="subheading">Our Humble Beginnings</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="timeline-inverted">
                                    <div class="timeline-image">
                                        <img class="rounded-circle img-fluid" src="{{asset('images/img/about/2.jpg')}}" alt="">
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4>March 2011</h4>
                                            <h4 class="subheading">An Agency is Born</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="timeline-image">
                                        <img class="rounded-circle img-fluid" src="{{asset('images/img/about/3.jpg')}}" alt="">
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4>December 2012</h4>
                                            <h4 class="subheading">Transition to Full Service</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="timeline-inverted">
                                    <div class="timeline-image">
                                        <img class="rounded-circle img-fluid" src="{{asset('images/img/about/4.jpg')}}" alt="">
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4>July 2014</h4>
                                            <h4 class="subheading">Phase Two Expansion</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="timeline-inverted">
                                    <div class="timeline-image">
                                        <h4>Be Part
                                            <br>Of Our
                                            <br>Story!</h4>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </section>

            <!-- Team -->
            <section class="bg-light cd-section" id="collaborators">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2 class="section-heading text-uppercase">Our Amazing Team</h2>
                            <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="grid">
                            <div class="col-sm-4">
                            <figure class="effect-phoebe rounded-circle">
                                <img src="{{asset('images/img/team/1.jpg')}}" class="rounded-circle" alt="img03"/>
                                <figcaption>

                                    <p class="team-socials">
                                        <a href="#"><i class="fab fa-twitter"></i></a>
                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#"><i class="fab fa-linkedin-in"></i></a>
                                    </p>
                                </figcaption>
                            </figure>
                                <h4>Kay Garland</h4>
                                <p class="text-muted">Lead Designer</p>
                            </div>
                            <div class="col-sm-4">
                            <figure class="effect-phoebe rounded-circle">
                                <img src="{{asset('images/img/team/2.jpg')}}" class="rounded-circle" alt="img07"/>
                                <figcaption>
                                    <p class="team-socials">
                                        <a href="#"><i class="fab fa-twitter"></i></a>
                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#"><i class="fab fa-linkedin-in"></i></a>
                                    </p>
                                </figcaption>
                            </figure>
                                <h4>Larry Parker</h4>
                                <p class="text-muted">Lead Marketer</p>
                            </div>
                            <div class="col-sm-4">
                                <figure class="effect-phoebe rounded-circle">
                                    <img src="{{asset('images/img/team/3.jpg')}}" class="rounded-circle" alt="img07"/>
                                    <figcaption>
                                        <p class="team-socials">
                                            <a href="#"><i class="fab fa-twitter"></i></a>
                                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                                            <a href="#"><i class="fab fa-linkedin-in"></i></a>
                                        </p>
                                    </figcaption>
                                </figure>
                                <h4>Diana Pertersen</h4>
                                <p class="text-muted">Lead Developer</p>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-lg-8 mx-auto text-center">
                            <p class="large text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
                        </div>
                    </div>
                </div>
            </section>


            <!-- Portfolio Modals -->

            <!-- Modal 1 -->
            <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="close-modal" data-dismiss="modal">
                            <div class="lr">
                                <div class="rl"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-8 mx-auto">
                                    <div class="modal-body">
                                        <!-- Project Details Go Here -->
                                        <h2 class="text-uppercase">Project Name</h2>
                                        <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                        <img class="img-fluid d-block mx-auto" src="{{asset('images/img/portfolio/01-full.jpg')}}" alt="">
                                        <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                        <ul class="list-inline">
                                            <li>Date: January 2017</li>
                                            <li>Client: Threads</li>
                                            <li>Category: Illustration</li>
                                        </ul>
                                        <button class="btn btn-primary" data-dismiss="modal" type="button">
                                            <i class="fas fa-times"></i>
                                            Close Project</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal 2 -->
            <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="close-modal" data-dismiss="modal">
                            <div class="lr">
                                <div class="rl"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-8 mx-auto">
                                    <div class="modal-body">
                                        <!-- Project Details Go Here -->
                                        <h2 class="text-uppercase">Project Name</h2>
                                        <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                        <img class="img-fluid d-block mx-auto" src="{{asset('images/img/portfolio/02-full.jpg')}}" alt="">
                                        <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                        <ul class="list-inline">
                                            <li>Date: January 2017</li>
                                            <li>Client: Explore</li>
                                            <li>Category: Graphic Design</li>
                                        </ul>
                                        <button class="btn btn-primary" data-dismiss="modal" type="button">
                                            <i class="fas fa-times"></i>
                                            Close Project</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal 3 -->
            <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="close-modal" data-dismiss="modal">
                            <div class="lr">
                                <div class="rl"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-8 mx-auto">
                                    <div class="modal-body">
                                        <!-- Project Details Go Here -->
                                        <h2 class="text-uppercase">Project Name</h2>
                                        <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                        <img class="img-fluid d-block mx-auto" src="{{asset('images/img/portfolio/03-full.jpg')}}" alt="">
                                        <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                        <ul class="list-inline">
                                            <li>Date: January 2017</li>
                                            <li>Client: Finish</li>
                                            <li>Category: Identity</li>
                                        </ul>
                                        <button class="btn btn-primary" data-dismiss="modal" type="button">
                                            <i class="fas fa-times"></i>
                                            Close Project</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal 4 -->
            <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="close-modal" data-dismiss="modal">
                            <div class="lr">
                                <div class="rl"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-8 mx-auto">
                                    <div class="modal-body">
                                        <!-- Project Details Go Here -->
                                        <h2 class="text-uppercase">Project Name</h2>
                                        <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                        <img class="img-fluid d-block mx-auto" src="{{asset('images/img/portfolio/04-full.jpg')}}" alt="">
                                        <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                        <ul class="list-inline">
                                            <li>Date: January 2017</li>
                                            <li>Client: Lines</li>
                                            <li>Category: Branding</li>
                                        </ul>
                                        <button class="btn btn-primary" data-dismiss="modal" type="button">
                                            <i class="fas fa-times"></i>
                                            Close Project</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal 5 -->
            <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="close-modal" data-dismiss="modal">
                            <div class="lr">
                                <div class="rl"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-8 mx-auto">
                                    <div class="modal-body">
                                        <!-- Project Details Go Here -->
                                        <h2 class="text-uppercase">Project Name</h2>
                                        <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                        <img class="img-fluid d-block mx-auto" src="{{asset('images/img/portfolio/05-full.jpg')}}" alt="">
                                        <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                        <ul class="list-inline">
                                            <li>Date: January 2017</li>
                                            <li>Client: Southwest</li>
                                            <li>Category: Website Design</li>
                                        </ul>
                                        <button class="btn btn-primary" data-dismiss="modal" type="button">
                                            <i class="fas fa-times"></i>
                                            Close Project</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal 6 -->
            <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="close-modal" data-dismiss="modal">
                            <div class="lr">
                                <div class="rl"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-8 mx-auto">
                                    <div class="modal-body">
                                        <!-- Project Details Go Here -->
                                        <h2 class="text-uppercase">Project Name</h2>
                                        <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                        <img class="img-fluid d-block mx-auto" src="{{asset('images/img/portfolio/06-full.jpg')}}" alt="">
                                        <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                        <ul class="list-inline">
                                            <li>Date: January 2017</li>
                                            <li>Client: Window</li>
                                            <li>Category: Photography</li>
                                        </ul>
                                        <button class="btn btn-primary" data-dismiss="modal" type="button">
                                            <i class="fas fa-times"></i>
                                            Close Project</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ONEPAGER -->

<div class="container">

                    @foreach($PageSection as $row)
                        {{--Sectiontype 0 = text only--}}
                        {{--Sectiontype 1 = text right--}}
                        {{--Sectiontype 2 = text left--}}
                        @if($row->sectiontype == 0)
                            <div class="row nomargin pagesection">
                                <div class="content-heading pull-right"><h2>{{$row->headline}} </h2></div>
                                <div class="span4">

                                    <div class="article-text-full pull-right">
                                        <p>
                                            {{$row->sectiontxt}}
                                        </p>
                                        <div class="readmore">
                                            <a class="readmore-btn" data-toggle="modal" data-target="#fsModal{{$row->id}}">Read more</a>
                                        </div>
                        @if(auth()->guest())
                        @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok pull-left">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('SectionController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('SectionController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                                    </div>
                                </div>
                            </div>
                        @elseif($row->sectiontype == 1)
                            <div class="row nomargin pagesection">
                                <div class="span4">
                                    <img class="img-left" src="{{$row->imgname_1}}"/>
                                    <div class="content-heading pull-right"><h2>{{$row->headline}} </h2></div>

                                    <div class="article-text pull-right">
                                        <p>
                                            {{$row->sectiontxt}}
                                        </p>
                                        <div class="readmore">
                                            <a class="readmore-btn" data-toggle="modal" data-target="#fsModal{{$row->id}}">Read more</a>
                                        </div>
                                        @if(auth()->guest())
                        @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok pull-right">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('SectionController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('SectionController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                                    </div>
                                </div>
                            </div>
                        @elseif($row->sectiontype == 2)
                            <div class="row nomargin pagesection">
                                <div class="span4">
                                    <img class="img-right" src="{{$row->imgname_1}}"/>
                                    <div class="content-heading pull-left"><h2>{{$row->headline}} </h2></div>
                                    <div class="article-text pull-left">
                                        <p>
                                            {{$row->sectiontxt}}
                                        </p>
                                        <div class="readmore">
                                            <a class="readmore-btn" data-toggle="modal" data-target="#fsModal{{$row->id}}">Read more</a>
                                        </div>
                                        @if(auth()->guest())
                        @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok pull-left">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('SectionController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('SectionController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                                <!-- modal -->
                            <div class="modal fade modal-fullscreen" id="fsModal{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content modal-content-one">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">{{$row->headline}}</h4>
                                        </div>
                                        <div class="modal-body">
                                            {!!$row->sectioncontent!!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- modal -->

                    @endforeach


</div>




@endsection