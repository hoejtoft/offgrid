<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('index');
//});
Route::get('/addsection', 'SectionController@addSection');
Route::resource('addnewsection','SectionController');
Auth::routes();

Route::get('/', 'SectionController@index');
Route::get('Offgrid','HomeController@pageOffgrid');
