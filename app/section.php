<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class section extends Model
{
    protected $table = 'section';
    protected $fillable =
        [
            'pagecategory',
            'sectiontype',
            'headline',
            'sectiontxt',
            'sectioncontent',
            'imgname_1',
            'buttonlink'
        ];
}
