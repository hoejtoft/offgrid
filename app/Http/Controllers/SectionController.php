<?php

namespace App\Http\Controllers;

use App\section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\Exception;

class SectionController extends Controller
{
    public function index()
    {
        $PageSection = DB::table('section')->where('pagecategory', '=', 'Frontpage')->get()->toArray();
        return view('index',compact('PageSection'));
    }
    public function addSection(){
        return view('addsection');
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'pagecategory' => 'required',
            'sectiontype' => 'required',
            'headline' => 'required',
            'sectiontxt' => 'required',
            'sectioncontent' => 'required',
            'imgname_1' => 'required',
            'buttonlink' => 'required',
        ]);
        if ($request->hasFile('imgname_1')) {
            $file = request()->file('imgname_1');
            $fileName = $file->getClientOriginalName();
            try{
                $file->move(public_path() . '/connect/uploads/images/', $fileName);
                $path = public_path() . "/connect/uploads/images/".$fileName;
            }
            catch(Exception $e){
                return $e;
            }
        }
        else{
            return "no file";
        }
        try{

            $pagesection = new section([
                'pagecategory' => $request->get('pagecategory'),
                'sectiontype' => $request->get('sectiontype'),
                'headline' => $request->get('headline'),
                'sectiontxt' => $request->get('sectiontxt'),
                'sectioncontent' => $request->get('sectioncontent'),
                'imgname_1' => $fileName,
                'buttonlink' => $request->get('buttonlink'),
            ]);
            $pagesection->save();
//                \Session::flash('success', 'New page section was created' );
            return redirect("/addsection")->with('success','New page section was created');




        }
        catch(\Exception $err){
            return "Error:" . $err->getMessage();
        }


    }
    public function update(Request $request,$id)
    {

        $pagesection = section::find($id);
        try{


            $this->validate($request,[
                'pagecategory' => 'required',
                'sectiontype' => 'required',
                'headline' => 'required',
                'sectiontxt' => 'required',
                'sectioncontent' => 'required',
                'imgname_1' => 'required',
                'buttonlink' => 'required',
            ]);
//            $newfile = $request->get('imgname_1');
//            if ($request->hasFile('imgname_1')) {
//                $file = request()->file('imgname_1');
//                $newfile = $file->getClientOriginalName();
//                try{
//                    $file->move(public_path() . '/connect/uploads/images/', $fileName);
//                    $path = public_path() . "/connect/uploads/images/".$fileName;
//                }
//                catch(Exception $e){
//                    return $e;
//                }
//            }

            $pagesection->pagecategory = $request->get('pagecategory');
            $pagesection->sectiontype = $request->get('sectiontype');
            $pagesection->headline = $request->get('headline');
            $pagesection->sectiontxt = $request->get('sectiontxt');
            $pagesection->sectioncontent = $request->get('sectioncontent');
            $pagesection->imgname_1 = $request->get('imgname_1');
            $pagesection->buttonlink = $request->get('buttonlink');

            $pagesection->save();


                return redirect("/")->with('success','New page section was created');





        }
        catch(\Exception $err){
            return "Error:" . $err->getMessage();
        }


    }
    public function edit($id)
    {

        $pagesection = section::find($id);
        return view('editsection',compact('pagesection','id'));
    }
    public function destroy($id)
    {

        $pagesection = section::find($id);

        $pagesection->delete();
        return redirect("/")->with('success','section deleted');
    }
}
