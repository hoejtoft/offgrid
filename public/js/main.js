$(document).ready(function(){
	$('.carousel').carousel();
	addnavbg();
	initfloatinput();
	initJodit();
	setDBContentByEditor();
	testpath();
	initAgencyjs();
	sidenav();
	fadeinMenu();
	runIt();
});

function addnavbg(){
	$(window).scroll(function() {
		var height = $(window).scrollTop();

		if(height  > 10) {
			$(".custom-nav").addClass('alt-color');
		}
		else{
			$(".custom-nav").removeClass('alt-color');

		}
	});
}
function initfloatinput(){
	(function($){
		function floatLabel(inputType){
			$(inputType).each(function(){
				var $this = $(this);
				var text_value = $(this).val();

				// on focus add class "active" to label
				$this.focus(function(){
					$this.next().addClass("active");
				});

				// on blur check field and remove class if needed
				$this.blur(function(){
					if($this.val() === '' || $this.val() === 'blank'){
						$this.next().removeClass();
					}
				});

				// Check input values on postback and add class "active" if value exists
				if(text_value!=''){
					$this.next().addClass("active");
				}
			});

			// Automatically remove floatLabel class from select input on load
			$( "select" ).next().removeClass();
		}
		// Add a class of "floatLabel" to the input field
		floatLabel(".floatLabel");
	})(jQuery);
}
function initJodit(){
var baseurl = window.location.protocol + "//" + window.location.host + "/";
	new Jodit('#editor', {
		enableDragAndDropFileToEditor: true,
		uploader: {
			url: baseurl+'connect/upload.php',
			format: 'json',
			pathVariableName: baseurl+'connect/uploads/images/',
			filesVariableName: 'images',
			prepareData: function (data) {
				return data;
			},
			isSuccess: function (resp) {
				return !resp.error;
			},
			getMsg: function (resp) {
				testpath();
				var imgpath = baseurl+"connect"+resp.images[0];
				testpath(imgpath);
				console.log("path: "+imgpath);
				return resp.msg.join !== undefined ? resp.msg.join(' ') : resp.msg;

			},
			process: function (resp) {

				return {
					files: resp[this.options.uploader.filesVariableName] || [],
					path: resp.path,
					baseurl: resp.baseurl,
					error: resp.error,
					msg: resp.msg
				};

			},
			error: function (e) {
				this.events.fire('errorPopap', [e.getMessage(), 'error', 4000]);
			},
			defaultHandlerSuccess: function (data, resp) {
				var i, field = this.options.uploader.filesVariableName;
				if (data[field] && data[field].length) {
					for (i = 0; i < data[field].length; i += 1) {
						this.selection.insertImage(data.baseurl + data[field][i]);
					}
				}
			},
			defaultHandlerError: function (resp) {
				this.events.fire('errorPopap', [this.options.uploader.getMsg(resp)]);
			}
		}
	});


}

function setDBContentByEditor(){
	$(".jodit_editor").keyup(function(){
		var data = $(".jodit_editor").html();
		$("#sectioncontent").val(data);
		console.log(data);
	})
}
function testpath(imgname){
	$("input[name='url']").addClass("Urlplaceholder");
	$(".Urlplaceholder").attr("placeholder",imgname);
	$(".Urlplaceholder").val(imgname);
}
function initAgencyjs(){
	(function($) {
		"use strict"; // Start of use strict

		// Smooth scrolling using jQuery easing
		$('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					$('html, body').animate({
						scrollTop: (target.offset().top - 54)
					}, 1000, "easeInOutExpo");
					return false;
				}
			}
		});

		// Closes responsive menu when a scroll trigger link is clicked
		$('.js-scroll-trigger').click(function() {
			$('.navbar-collapse').collapse('hide');
		});

		// Activate scrollspy to add active class to navbar items on scroll
		$('body').scrollspy({
			target: '.custom-nav',
			offset: 56
		});

		// Hide navbar when modals trigger
		$('.portfolio-modal').on('show.bs.modal', function(e) {
			$('.navbar').addClass('d-none');
		})
		$('.portfolio-modal').on('hidden.bs.modal', function(e) {
			$('.navbar').removeClass('d-none');
		})

	})(jQuery); // End of use strict

}
function sidenav(){
	var contentSections = $('.cd-section'),
		navigationItems = $('#cd-vertical-nav a');

	updateNavigation();
	$(window).on('scroll', function(){
		updateNavigation();
	});

	//smooth scroll to the section
	navigationItems.on('click', function(event){
		event.preventDefault();
		smoothScroll($(this.hash));
	});
	//smooth scroll to second section
	$('.cd-scroll-down').on('click', function(event){
		event.preventDefault();
		smoothScroll($(this.hash));
	});

	//open-close navigation on touch devices
	$('.cd-nav-trigger').on('click', function(){
		$('#cd-vertical-nav').toggleClass('open');

	});
	//close navigation on touch devices when selectin an elemnt from the list
	$('#cd-vertical-nav a').on('click', function(){
		$('#cd-vertical-nav').removeClass('open');
	});

	function updateNavigation() {
		contentSections.each(function(){
			$this = $(this);
			var activeSection = $('#cd-vertical-nav a[href="#'+$this.attr('id')+'"]').data('number') - 1;
			if ( ( $this.offset().top - $(window).height()/2 < $(window).scrollTop() ) && ( $this.offset().top + $this.height() - $(window).height()/2 > $(window).scrollTop() ) ) {
				navigationItems.eq(activeSection).addClass('is-selected');
			}else {
				navigationItems.eq(activeSection).removeClass('is-selected');
			}
		});
	}

	function smoothScroll(target) {
		var offset = 0;
		$('body,html').animate(
			{'scrollTop':target.offset().top - offset},
			900
		);
	}
}
function fadeinMenu(){
	$(".hidelinks").fadeOut(0);
	$(".logo").mouseover(function(){
		$(".hidelinks").fadeIn(500);
		$(".custom-nav").css("background-color","#ffffff");
	});
	$(".custom-nav").mouseleave(function(){
		$(".hidelinks").fadeOut(500);
		$(".custom-nav").css("background-color","#ffffff00");
	});
}
function runIt() {
	var arrow = $('.cd-scroll-down');
	arrow.animate({opacity:'1'}, 2000);
	arrow.animate({opacity:'0.5'}, 2000, runIt);
}


